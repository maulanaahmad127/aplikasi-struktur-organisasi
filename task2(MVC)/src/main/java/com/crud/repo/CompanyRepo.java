package com.crud.repo;

import com.crud.model.Company;

import org.springframework.data.repository.CrudRepository;

public interface CompanyRepo extends CrudRepository<Company, Long> {

    
}
