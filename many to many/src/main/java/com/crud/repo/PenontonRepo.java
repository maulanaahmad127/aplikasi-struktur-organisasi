package com.crud.repo;

import com.crud.model.Penonton;

import org.springframework.data.repository.CrudRepository;

public interface PenontonRepo extends CrudRepository<Penonton, Integer> {
    
}
