package com.crud.repo;

import com.crud.model.KecamatanDesa;

import org.springframework.data.repository.CrudRepository;

public interface KecamatanDesaRepo extends CrudRepository<KecamatanDesa, Long>{
    
}
