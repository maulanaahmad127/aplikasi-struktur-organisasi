package com.crud.repo;

import com.crud.model.Kota;

import org.springframework.data.repository.CrudRepository;

public interface KotaRepo extends CrudRepository<Kota, Long> {

    
}
