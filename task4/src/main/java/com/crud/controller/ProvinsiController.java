package com.crud.controller;

import com.crud.model.KecamatanDesa;
import com.crud.model.Kota;
import com.crud.model.Provinsi;
import com.crud.service.KecamatanDesaService;
import com.crud.service.KotaService;
import com.crud.service.ProvinsiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ProvinsiController {

    @Autowired
    private ProvinsiService service;

    @Autowired
    private KotaService kotaService;

    @Autowired
    private KecamatanDesaService desaService;
    
    @GetMapping
    public String home(Model model){
        model.addAttribute("msg", "Welcome To Provinsi CRUD");
        model.addAttribute("provinsi", service.findAll());
        model.addAttribute("kota", kotaService.findAll());
        model.addAttribute("kecdes", desaService.findAll());
        return "index";
    }

    @GetMapping("/add")
    public String add(Model model){
        model.addAttribute("msg", "Welcome To Provinsi CRUD");
        model.addAttribute("provinsi", new Provinsi());
        return "add";
    }

    @GetMapping("/addKota")
    public String addKota(Model model){
        model.addAttribute("msg", "Welcome To Provinsi CRUD");
        model.addAttribute("kota", new Kota());
        return "addKota";
    }

    @GetMapping("/addKecDes")
    public String addKecDes(Model model){
        model.addAttribute("msg", "Welcome To Provinsi CRUD");
        model.addAttribute("kecdes", new KecamatanDesa());
        return "addKecDes";
    }

    @PostMapping("/save")
    public String save(Model model, Provinsi provinsi){
        model.addAttribute("msg", "Welcome To Employee CRUD");
        model.addAttribute("provinsi", provinsi);
        service.add(provinsi);
        model.addAttribute("provinsi", service.findAll());
        return "redirect:/";
    }

    @PostMapping("/saveKota")
    public String saveKota(Model model, Kota kota){
        model.addAttribute("msg", "Welcome To Employee CRUD");
        model.addAttribute("kota", kota);
        kotaService.add(kota);
        model.addAttribute("kota", kotaService.findAll());
        return "redirect:/";
    }

    @PostMapping("/saveKecDes")
    public String saveKecDes(Model model, KecamatanDesa kecdes){
        model.addAttribute("msg", "Welcome To Employee CRUD");
        model.addAttribute("kecdes", kecdes);
        desaService.add(kecdes);
        model.addAttribute("kota", kotaService.findAll());
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id){
        service.delete(id);
        return "redirect:/";
    }

    @GetMapping("/deleteKota/{id}")
    public String deleteKota(@PathVariable("id") long id){
        kotaService.delete(id);
        return "redirect:/";
    }

    @GetMapping("/deleteKecDes/{id}")
    public String deleteKecDes(@PathVariable("id") long id){
        desaService.delete(id);
        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") long id, Model model, Provinsi provinsi){
        model.addAttribute("provinsi", service.findById(id));
        
        return "edit";
    }

    @GetMapping("/updateKota/{id}")
    public String updateKota(@PathVariable("id") long id, Model model, Provinsi provinsi){
        model.addAttribute("kota", kotaService.findById(id));
        return "editKota";
    }

    @GetMapping("/updateKecDes/{id}")
    public String updateKecDes(@PathVariable("id") long id, Model model, KecamatanDesa kecDes){
        model.addAttribute("kecdes", desaService.findById(id));
        return "editKecDes";
    }

    @PostMapping("/update")
    public String update(Model model, Provinsi provinsi){
        service.update(provinsi);
        return "redirect:/";
    }

    @PostMapping("/updateKota")
    public String update(Model model, Kota kota){
        kotaService.update(kota);
        return "redirect:/";
    }

    @PostMapping("/updateKecDes")
    public String updateKecDes(Model model, KecamatanDesa kecdes){
        desaService.update(kecdes);
        return "redirect:/";
    }

    
}
