package com.crud.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_penonton")
public class Penonton {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String pembayaran;
    
    
    @ManyToMany( fetch = FetchType.LAZY)
    @JoinTable(name = "tbl_penonton_film",
    joinColumns = @JoinColumn(name = "penonton_id"),
    inverseJoinColumns = @JoinColumn(name = "film_id")
    )
    private Set<Film> films;

    

    public Set<Film> getFilms() {
        return films;
    }
    public void setFilms(Set<Film> films) {
        this.films = films;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getPembayaran() {
        return pembayaran;
    }
    public void setPembayaran(String pembayaran) {
        this.pembayaran = pembayaran;
    }
    
   

}
