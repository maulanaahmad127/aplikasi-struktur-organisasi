package com.crud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "kota")
public class Kota {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String nama;

    @ManyToOne
    private Provinsi provinsi;

    public long getId() {
        return id;
    }
    public void setId(long l) {
        this.id = l;
    }
    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }

    public long getProvinsiId(){
        return provinsi.getId();
    }

    public String getProvinsiName(){
        return provinsi.getNama();
    }
    public Provinsi getProvinsi() {
        return provinsi;
    }
    public void setProvinsi(Provinsi provinsi) {
        this.provinsi = provinsi;
    }
    
}
