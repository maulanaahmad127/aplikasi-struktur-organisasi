package com.crud.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "kecdes")
public class KecamatanDesa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String namaKecamatan;
    private String namaDesa;

    @ManyToOne
    private Kota kota;

    public long getId() {
        return id;
    }
    public void setId(long l) {
        this.id = l;
    }
    public String getNamaKecamatan() {
        return namaKecamatan;
    }
    public void setNamaKecamatan(String namaKecamatan) {
        this.namaKecamatan = namaKecamatan;
    }
    public String getNamaDesa() {
        return namaDesa;
    }
    public void setNamaDesa(String namaDesa) {
        this.namaDesa = namaDesa;
    }
    public Kota getKota() {
        return kota;
    }
    public void setKota(Kota kota) {
        this.kota = kota;
    }
    public long getKotaId(){
        return kota.getId();
    }
    public String getKotaName(){
        return kota.getNama();
    }
    

    
    
}
