package com.crud.controller;

import com.crud.model.Company;
import com.crud.service.CompanyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class controller {

    @Autowired
    private CompanyService service;
    
    @GetMapping
    public String home(Model model){
        model.addAttribute("msg", "Welcome To Company CRUD");
        model.addAttribute("companies", service.findAll());
        
        return "index";
    }

    @GetMapping("/add")
    public String add(Model model){
        model.addAttribute("msg", "Welcome To Company CRUD");
        model.addAttribute("company", new Company());
        return "add";
    }

    @PostMapping("/save")
    public String save(Model model, Company company){
        model.addAttribute("msg", "Welcome To company CRUD");
        model.addAttribute("company", company);
        service.add(company);
        model.addAttribute("companies", service.findAll());
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id){
        service.delete(id);
        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") long id, Model model, Company company){
        model.addAttribute("company", service.findById(id));
        
        return "edit";
    }

    @PostMapping("/update")
    public String update(Model model, Company employee){
        
        service.update(employee);
        System.out.println(employee.getId());
        return "redirect:/";
    }

    
}
