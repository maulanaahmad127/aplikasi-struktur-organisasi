package com.crud.repo;

import com.crud.model.Provinsi;

import org.springframework.data.repository.CrudRepository;

public interface ProvinsiRepo extends CrudRepository<Provinsi, Long> {

    
}
