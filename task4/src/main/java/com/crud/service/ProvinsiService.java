package com.crud.service;


import java.util.Optional;

import com.crud.model.Provinsi;
import com.crud.repo.ProvinsiRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProvinsiService {

    @Autowired
    private ProvinsiRepo repo;


    public Iterable<Provinsi> findAll(){
        return repo.findAll();
    }

    public Optional<Provinsi> findById(long id){
       return repo.findById(id);
    }

    public void add(Provinsi provinsi){
        repo.save(provinsi);
    }

    public void update(Provinsi provinsi){
        repo.save(provinsi);
    }

    public void delete(long id){
        repo.deleteById(id);
    }
}
