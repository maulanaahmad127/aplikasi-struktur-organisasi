package com.crud.service;


import java.util.Optional;

import com.crud.model.KecamatanDesa;
import com.crud.repo.KecamatanDesaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KecamatanDesaService {

    @Autowired
    private KecamatanDesaRepo repo;


    public Iterable<KecamatanDesa> findAll(){
        return repo.findAll();
    }

    public Optional<KecamatanDesa> findById(long id){
       return repo.findById(id);
    }

    public void add(KecamatanDesa desa){
        repo.save(desa);
    }

    public void update(KecamatanDesa desa){
        repo.save(desa);
    }

    public void delete(long id){
        repo.deleteById(id);
    }
}
