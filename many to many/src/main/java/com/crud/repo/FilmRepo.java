package com.crud.repo;

import java.util.List;

import com.crud.model.Film;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface FilmRepo extends CrudRepository<Film, Integer> {
    
    @Query("select title from Film")
    public List<String> findAllByTitle();
}
