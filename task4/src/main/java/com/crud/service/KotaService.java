package com.crud.service;


import java.util.Optional;
import com.crud.model.Kota;
import com.crud.repo.KotaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KotaService {

    @Autowired
    private KotaRepo repo;


    public Iterable<Kota> findAll(){
        return repo.findAll();
    }

    public Optional<Kota> findById(long id){
       return repo.findById(id);
    }

    public void add(Kota kota){
        repo.save(kota);
    }

    public void update(Kota kota){
        repo.save(kota);
    }

    public void delete(long id){
        repo.deleteById(id);
    }
}
