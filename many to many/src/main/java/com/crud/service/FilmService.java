package com.crud.service;

import java.util.Optional;

import com.crud.model.Film;
import com.crud.repo.FilmRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FilmService {
    
    @Autowired
    private FilmRepo repo;

    public Iterable<Film> findAll() {
        return repo.findAll();
    }

    public Film save(Film film){
        return repo.save(film);
    }

    public void deleteById(int id){
         repo.deleteById(id);
    }

    public Optional<Film> findById(int id){
        return repo.findById(id);
    }
}
