package com.crud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String nama;

    @Column(columnDefinition = "varchar(255) default 'Staff'")
    private String posisi;

    @ManyToOne(optional = true)
   private Company company;



    public Employee(){

    }
   

    public int getId() {
        return id;
    }
    public void setId(int l) {
        this.id = l;
    }
    public String getNama() {
        return nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }

    


    public Company getCompany() {
        return company;
    }


    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCompanyName(){
        return company.getNama();
    }


    public String getPosisi() {
        return posisi;
    }


    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }
   
    public int getCompanyId(){
        return company.getId();
    }

    
    
}
